package com.Pong3D.game;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

import android.util.Log;

/*
 * Holding class for player.
 * Each paddle has a player assigned to it, the player holds the number of lives, and can draw them
 */
public class Player
{
    private int playerID;
    private int nLives;
    private float[] nColor;

    public Player(int _playerID)
    {
        // Set number of lives
        this.playerID = _playerID;
        this.nLives = 3;
        // If playerID is 1, then player is Human
        if (playerID == 1)
        {
            // Set color to be blue
            float[] tcolor =
            { 0.0f, 0.0f, 1.0f, 1.0f };
            nColor = tcolor;
        }
        else
        {

            // Set color to be red
            float[] tcolor =
            { 1.0f, 0.0f, 0.0f, 1.0f };
            nColor = tcolor;
        }
    }

    public int getLives()
    {
        return nLives;
    }

    public void setLives(int nLives)
    {
        this.nLives = nLives;
    }

    public void loseLife()
    {
        Log.v("Player" + playerID, "life Lost");
        // Take away a life
        nLives -= 1;

    }

    /*
     * returns true if the player still has lives left, false if not
     */
    public boolean isAlive()
    {
        if (nLives == 0)
            return false;
        else
            return true;
    }

    public int getPlayerID()
    {
        return playerID;
    }

    public void setPlayerID(int playerID)
    {
        this.playerID = playerID;
    }

    public void drawlives(GL10 gl)
    {
        float[] verticea = null;
        gl.glRotatef(0, 1, 1, 1);// Rotate the shapes to be in line with the original Draw
        gl.glTranslatef(0.0f, 0.0f, 0.0f);
        for (float i = 1; i < nLives; i++)
        {
            if (playerID == 1)
            {
                float[] vertices =
                { -(i / 2), 1.8f, 0f, -(i / 2), 2.0f, 0f, -(i / 2) - 0.2f, 1.8f, 0f, -(i / 2) - 0.2f, 2.0f, 0f };
                verticea = vertices;
            }
            else
            {
                float[] vertices =
                { (i / 2), 1.8f, 0f, (i / 2), 2.0f, 0f, -(i / 2) - 0.2f, 1.8f, 0f, (i / 2) - 0.2f, 2.0f, 0f };
                verticea = vertices;
            }

            ByteBuffer vbb = ByteBuffer.allocateDirect(verticea.length * 4);

            vbb.order(ByteOrder.nativeOrder()); // Use native byte order
            FloatBuffer vertexBuffer = vbb.asFloatBuffer(); // Convert from byte to float
            vertexBuffer.put(verticea); // Copy data into buffer

            vertexBuffer.position(0); // Rewind

            // Enable vertex-array and define its buffer
            gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
            gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBuffer);

            // Draw the primitives from the vertex-array directly

            // Set the color for the Lives
            gl.glColor4f(nColor[0], nColor[1], nColor[2], nColor[3]); // Set in constructor

            gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, verticea.length / 3);

            gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
        }
    }
}
