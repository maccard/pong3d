package com.Pong3D.game;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.view.KeyEvent;

/*
 * This is only used for OnKey event
 */
public class MyGLSurfaceView extends GLSurfaceView
{
    Render renderer; // Custom GL Renderer

    // Constructor - Allocate and set the renderer
    public MyGLSurfaceView(Context context, Game _game, Render _r)
    {
        super(context);

        // User our custom renderer
        renderer = _r;
        this.setRenderer(renderer);

        // Request focus, otherwise key/button won't react
        this.requestFocus();
        this.setFocusableInTouchMode(true);

    }

    // Handler for key event
    // Return true if event handled
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent evt)
    {
        if (keyCode == KeyEvent.KEYCODE_DPAD_LEFT)
        {
            renderer.getGame().paddle_1.moveleft();
            return true; // Event handled
        }
        else
        {
            if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT)
            {
                renderer.getGame().paddle_1.moveright();
                return true; // Event handled
            }
            else
            {
                if (keyCode == KeyEvent.KEYCODE_DPAD_UP)
                {
                    renderer.getGame().paddle_1.moveup();
                    return true; // Event handled
                }
                else
                    if (keyCode == KeyEvent.KEYCODE_DPAD_DOWN)
                    {
                        renderer.getGame().paddle_1.movedown();
                        return true; // Event handled
                    }
            }
        }
        /*
         * Control apddle 6 with WASD
         */
        if (keyCode == KeyEvent.KEYCODE_A)
        {
            renderer.getGame().paddle_6.moveleft();
            return true; // Event handled
        }
        else
        {
            if (keyCode == KeyEvent.KEYCODE_D)
            {
                renderer.getGame().paddle_6.moveright();
                return true; // Event handled
            }
            else
            {
                if (keyCode == KeyEvent.KEYCODE_W)
                {
                    renderer.getGame().paddle_6.moveup();
                    return true; // Event handled
                }
                else
                    if (keyCode == KeyEvent.KEYCODE_S)
                    {
                        renderer.getGame().paddle_6.movedown();
                        return true; // Event handled
                    }
            }
        }
        // Toggle Paddle 6 AI with T,
        // Toggle Paddle 1 AI with Y
        if (keyCode == KeyEvent.KEYCODE_T)
        {
            if (renderer.getGame().paddle_6.getPaddle_state() == 0)
                renderer.getGame().paddle_6.setPaddle_state(1);
            else
                renderer.getGame().paddle_6.setPaddle_state(0);
        }
        if (keyCode == KeyEvent.KEYCODE_Y)
        {
            if (renderer.getGame().paddle_1.getPaddle_state() == 0)
                renderer.getGame().paddle_1.setPaddle_state(1);
            else
                renderer.getGame().paddle_1.setPaddle_state(0);
        }
        return false;// Didn't handle event
    }
}