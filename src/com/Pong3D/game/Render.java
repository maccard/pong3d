package com.Pong3D.game;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.opengl.GLSurfaceView;
import android.opengl.GLU;

/*
 * This is the main game file, where all the updated draw files will be called, for recalculated ball positions, etc
 */

public class Render implements GLSurfaceView.Renderer
{
    private Game game;

    // Rotation Angles
    public float anglex = 30;
    public float angley = 30;
    public float anglez = 0;
    // These are the target angles
    public float newx = 30;
    public float newy = 30;
    public float newz = 0;

    // Speed of rotation
    public float rspeed = 10;

    boolean blendingEnabled = false; // Blending toggle

    // Constructor
    public Render(Game _game)
    {
        // Create New Game Object
        setGame(_game);
    }

    // Call back when the surface is first created or re-created
    // @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
        gl.glClearColor(0.01f, 0.01f, 0.01f, 1.0f); // Set color's clear-value to
        // black
        gl.glClearDepthf(1.0f); // Set depth's clear-value to farthest
        gl.glEnable(GL10.GL_DEPTH_TEST); // Enables depth-buffer for hidden
        // surface removal
        gl.glDepthFunc(GL10.GL_LEQUAL); // The type of depth testing to do
        gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, GL10.GL_NICEST); // nice
        // perspective
        // view
        gl.glShadeModel(GL10.GL_SMOOTH); // Enable smooth shading of color
        gl.glDisable(GL10.GL_DITHER); // Disable dithering for better
        // performance

    }

    // Call back after onSurfaceCreated() or whenever the window's size changes
    // @Override
    public void onSurfaceChanged(GL10 gl, int width, int height)
    {
        if (height == 0)
            height = 1; // To prevent divide by zero
        float aspect = (float) width / height;
        // Set the viewport (display area) to cover the entire window
        gl.glViewport(0, 0, width, height);
        // Setup perspective projection, with aspect ratio matches viewport
        gl.glMatrixMode(GL10.GL_PROJECTION); // Select projection matrix
        gl.glLoadIdentity(); // Reset projection matrix
        // Use perspective projection
        GLU.gluPerspective(gl, 45, aspect, 0.1f, 100.f);
        gl.glMatrixMode(GL10.GL_MODELVIEW); // Select model-view matrix

        // Setup Blending
        gl.glColor4f(1.0f, 1.0f, 1.0f, 0.5f); // Full brightness, 50% alpha
        gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE); // Select blending function

        gl.glLoadIdentity(); // Reset
    }

    // Call back to draw the current frame.
    // @Override
    public void onDrawFrame(GL10 gl)
    {
        // Updates game
        game.updateGame();
        // Clear color and depth buffers
        gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);

        // Blending Enabled? (NEW)
        if (blendingEnabled)
        {
            gl.glEnable(GL10.GL_BLEND); // Turn blending on (NEW)
            gl.glDisable(GL10.GL_DEPTH_TEST); // Turn depth testing off (NEW)

        }
        else
        {
            gl.glDisable(GL10.GL_BLEND); // Turn blending off (NEW)
            gl.glEnable(GL10.GL_DEPTH_TEST); // Turn depth testing on (NEW)
        }

        gl.glBlendFunc(GL10.GL_SRC_ALPHA_SATURATE, GL10.GL_ONE);
        gl.glHint(GL10.GL_POLYGON_SMOOTH_HINT, GL10.GL_NICEST); // no visible diff
        gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, GL10.GL_NICEST);
        // ----- Render the Wireframe Cube -----
        gl.glLoadIdentity(); // Reset the model-view matrix
        gl.glTranslatef(0.0f, 0.0f, -6.0f); // Translate into the screen

        // Draw Lives
        game.player1.drawlives(gl);

        // Rotations in X, Y, Z Planes
        rotateCube(gl);

        gl.glScalef(1.0f, 1.0f, 1.0f); // Scale down

        // Draw the cube, and each paddle
        game.drawGame(gl);
    }

    public void rotateCube(GL10 gl)
    {
        // Update necessary angles.
        if (newx >= anglex + rspeed)
            anglex += rspeed;

        if (newy >= angley + rspeed)
            angley += rspeed;

        if (newz >= anglez + rspeed)
            anglez += rspeed;

        if (newx <= anglex - rspeed)
            anglex -= rspeed;

        if (newy <= angley - rspeed)
            angley -= rspeed;

        if (newz <= anglez - rspeed)
            anglez -= rspeed;

        // Perform the rotations
        gl.glRotatef(this.anglex, 1.0f, 0.0f, 0.0f); // Rotate Cube, X
        gl.glRotatef(this.angley, 0.0f, 1.0f, 0.0f); // Rotate Cube, Y
        gl.glRotatef(this.anglez, 0.0f, 0.0f, 1.0f); // Rotate Cube, Z
    }

    /**
     * @return the game
     */
    public Game getGame()
    {
        return game;
    }

    /**
     * @param game
     *            the game to set
     */
    public void setGame(Game game)
    {
        this.game = game;
    }

}
