package com.Pong3D.game;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

/*
 * Class that contains Methods for the paddle, such as returning coordinates, movement,
 */
public class Paddle extends Cube
{
    // Player that the paddle is assigned to, either 1 or 2
    private Player pPlayer;

    // Width Thickness and velocity of the paddle
    private float w = 0.12f, t = 0.01f, vel = 0.2f;

    private int or = -1;
    /*
     * Orientation of paddle. 0 is top or bottom,
     * 1 is left or right,
     * 2 is front or back
     */

    private float X, Y, Z; // X Y Z coordinates of the paddle

    /*
     * All three coordinates are the points at which the paddle is drawn at on the calling of the render
     */
    private int paddle_state = -1;
    private FloatBuffer vertexBuffer; // Buffer for vertex-array
    private int numFaces = 6;

    private float[][] colors;
    private float[] vertices =
    {
            // Vertices of the 6 faces
            // FRONT
            -1.0f, -1.0f, 1.0f, // 0. left-bottom-front
            1.0f, -1.0f, 1.0f, // 1. right-bottom-front
            -1.0f, 1.0f, 1.0f, // 2. left-top-front
            1.0f, 1.0f, 1.0f, // 3. right-top-front
            // BACK
            1.0f, -1.0f, -1.0f, // 6. right-bottom-back
            -1.0f, -1.0f, -1.0f, // 4. left-bottom-back
            1.0f, 1.0f, -1.0f, // 7. right-top-back
            -1.0f, 1.0f, -1.0f, // 5. left-top-back
            // LEFT
            -1.0f, -1.0f, -1.0f, // 4. left-bottom-back
            -1.0f, -1.0f, 1.0f, // 0. left-bottom-front
            -1.0f, 1.0f, -1.0f, // 5. left-top-back
            -1.0f, 1.0f, 1.0f, // 2. left-top-front
            // RIGHT
            1.0f, -1.0f, 1.0f, // 1. right-bottom-front
            1.0f, -1.0f, -1.0f, // 6. right-bottom-back
            1.0f, 1.0f, 1.0f, // 3. right-top-front
            1.0f, 1.0f, -1.0f, // 7. right-top-back
            // TOP
            -1.0f, 1.0f, 1.0f, // 2. left-top-front
            1.0f, 1.0f, 1.0f, // 3. right-top-front
            -1.0f, 1.0f, -1.0f, // 5. left-top-back
            1.0f, 1.0f, -1.0f, // 7. right-top-back
            // BOTTOM
            -1.0f, -1.0f, -1.0f, // 4. left-bottom-back
            1.0f, -1.0f, -1.0f, // 6. right-bottom-back
            -1.0f, -1.0f, 1.0f, // 0. left-bottom-front
            1.0f, -1.0f, 1.0f // 1. right-bottom-front
    };

    // Centre point, and either vertical or horizontal at that point
    // enum_state toggles player or CPU controlled. 1 is cpu, 0 is user controlled
    public Paddle(float x, float y, float z, int _or, int enum_state)
    {
        this.updatePaddle(x, y, z);
        this.or = _or;
        this.setVertices();

        paddle_state = enum_state;

        ByteBuffer vbb = ByteBuffer.allocateDirect(vertices.length * 4);
        vbb.order(ByteOrder.nativeOrder()); // Use native byte order
        vertexBuffer = vbb.asFloatBuffer(); // Convert from byte to float
        vertexBuffer.put(vertices); // Copy data into buffer
        vertexBuffer.position(0); // Return buffer back to beginning
    }

    // This function checks the orientation of the paddle specified, and maps a
    // 3d XYZ coordinate system to a 2D one.
    // It also only deals with autonomous control
    public void updatePaddle(float x, float y, float z)
    {
        // These conditions restrict movement to the range of the cube.
        // if any of the values entered fall outside the range of the cube, then
        // don't let the ball move
        // This condition will only be met on first calling. This draws all
        // three points.
        if (this.or == -1)
        {
            this.X = x;
            this.Y = y;
            this.Z = z;
            return;
        }

        // If these conditions are checked, then the paddle will be
        // automagically in line with the ball

        // Top or bottom paddle
        float nx = x, ny = y, nz = z;

        if (this.or == 0)
        {
            // Top & bottom paddles move in X-Z plane
            if (Math.abs(nz) + this.w <= 1.0f)
                this.Z = nz;
            if (Math.abs(nx) + this.w <= 1.0f)
                this.X = nx;
            return;
        }
        // Left or right Paddle
        if (this.or == 1)
        {
            // Left & right paddles move in Y-Z Plane
            if (Math.abs(nz) + this.w <= 1.0f)
                this.Z = nz;
            if (Math.abs(ny) + this.w <= 1.0f)
                this.Y = ny;
            return;
        }
        // Front or back paddle
        if (this.or == 2)
        {
            // Front & back paddles move in X-Y Plane
            if (Math.abs(ny) + this.w <= 1.0f)
                this.Y = ny;
            if (Math.abs(nx) + this.w <= 1.0f)
                this.X = nx;
            return;
        }
    }

    /*
     * This is the set of move functions necessary for the manual control of the paddle. each one checks that the move is legal(stays within
     * cube bounds) and then implements the move if it is.
     */
    public void moveup()
    {
        if (paddle_state == 1)
            // paddle is CPU controlled
            return;
        // Debug
        // Log.v("X", " " + this.X);
        // Log.v("Y", " " + this.Y);
        // Log.v("Z", " " + this.Z);
        if (this.or == 0 && (this.Z - this.vel > -1.0f))// Top/Bottom
            this.Z -= this.vel;
        if (this.or == 1 && (this.Y + this.vel < 1.0f))// Left/Right
            this.Y += this.vel;
        if (this.or == 2 && (this.Y + this.vel < 1.0f))// Back/Front
            this.Y += this.vel;
    }

    public void movedown()
    {
        if (paddle_state == 1)
            // paddle is CPU controlled
            return;

        // Debug
        // Log.v("X", " " + this.X);
        // Log.v("Y", " " + this.Y);
        // Log.v("Z", " " + this.Z);
        if (this.or == 0 && (this.Z + this.w + this.vel < 1.0f))// Top/Bottom
            this.Z += this.vel;
        if (this.or == 1 && (this.Y - this.w - this.vel > -1.0f))// Left/Right
            this.Y -= this.vel;
        if (this.or == 2 && (this.X - this.w - this.vel > -1.0f))// Back/Front
            this.Y -= this.vel;

    }

    public void moveleft()
    {
        if (paddle_state == 1)
            // paddle is CPU controlled
            return;
        // Debug
        // Log.v("X", " " + this.X);
        // Log.v("Y", " " + this.Y);
        // Log.v("Z", " " + this.Z);
        if (this.or == 0 && (this.X - this.w - this.vel > -1.0f))// Top/Bottom
            this.X -= this.vel;
        if (this.or == 1 && (this.Z - this.w - this.vel > -1.0f))// Left/right
            this.Z -= this.vel;
        if (this.or == 2 && (this.X - this.w - this.vel > -1.0f))// Back/Front
            this.X -= this.vel;
    }

    public void moveright()
    {
        if (paddle_state == 1)
            // paddle is CPU controlled
            return;
        // Debug
        // Log.v("X", " " + this.X);
        // Log.v("Y", " " + this.Y);
        // Log.v("Z", " " + this.Z);
        if (this.or == 0 && (this.X + this.w + this.vel < 1.0f))// Top/bottom
            this.X += this.vel;
        if (this.or == 1 && (this.Z + this.w + this.vel < 1.0f))// Left/right
            this.Z += this.vel;
        if (this.or == 2 && (this.X + this.w + this.vel < 1.0f))// Back/front
            this.X += this.vel;

    }

    // Draw the shape
    public void draw(GL10 gl)
    {
        // Set up new vertices based on new centrepoint
        this.setVertices();
        vertexBuffer.put(vertices); // Copy data into buffer
        vertexBuffer.position(0); // Rewind
        gl.glFrontFace(GL10.GL_CCW);// Orientate Counter Clockwise

        gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
        gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBuffer);

        // Render all the faces of the paddle
        for (int face = 0; face < numFaces; face++)
        {
            // Set the color for each of the faces
            gl.glColor4f(colors[face][0], colors[face][1], colors[face][2], colors[face][3]);
            // Draw the primitive from the vertex-array directly
            gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, face * 4, 4);
        }

        gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
        gl.glDisable(GL10.GL_CULL_FACE);
    }

    /*
     * Sets the new vertices of the paddles. The orientation is checked, and based on the orientation the paddle is drawn. Math behind this
     * can be produced on request
     * This is also v. inefficient, as it's setting up an array of 72 floats, and then copying it over an
     * existing array of floats
     * Optimisitaions: return the new array of floats, and store that in the current array of floats, or do this
     * calculation in the draw function, no storage of vertices necessary, and the points can just be passed into the buffer directly
     * This is also hard on the eyes.
     */
    private void setVertices()
    {
        // Top or bottom paddle
        if (this.or == 0)
        {
            float[] _temp =
            {
                    // Vertices of the 6 faces
                    // FRONT
                    this.X - this.w, this.Y, this.Z + this.w, // 0.
                    // left-bottom-front
                    this.X + this.w, this.Y, this.Z + this.w, // 1.
                    // right-bottom-front
                    this.X + this.w, this.Y + this.t, this.Z + this.w, // 3.
                    // right-top-front
                    this.X - this.w, this.Y + this.t, this.Z + this.w, // 2.
                    // left-top-front
                    // BACK
                    this.X + this.w, this.Y, this.Z - this.w, // 6.
                    // right-bottom-back
                    this.X - this.w, this.Y, this.Z - this.w, // 4.
                    // left-bottom-back
                    this.X + this.w, this.Y + this.t, this.Z - this.w, // 7.
                    // right-top-back
                    this.X - this.w, this.Y + this.t, this.Z - this.w, // 5.
                    // left-top-back
                    // LEFT
                    this.X - this.w, this.Y, this.Z - this.w, // 4.
                    // left-bottom-back
                    this.X - this.w, this.Y, this.Z + this.w, // 0.
                    // left-bottom-front
                    this.X - this.w, this.Y + this.t, this.Z - this.w, // 5.
                    // left-top-back
                    this.X - this.w, this.Y + this.t, this.Z + this.w, // 2.
                    // left-top-front
                    // RIGHT
                    this.X + this.w, this.Y, this.Z + this.w, // 1.
                    // right-bottom-front
                    this.X + this.w, this.Y, this.Z - this.w, // 6.
                    // right-bottom-back
                    this.X + this.w, this.Y + this.t, this.Z + this.w, // 3.
                    // right-top-front
                    this.X + this.w, this.Y + this.t, this.Z - this.w, // 7.
                    // right-top-back
                    // TOP
                    this.X - this.w, this.Y + this.t, this.Z - this.w, // 5.
                    // left-top-back
                    this.X - this.w, this.Y + this.t, this.Z + this.w, // 2.
                    // left-top-front
                    this.X + this.w, this.Y + this.t, this.Z - this.w, // 7.
                    // right-top-back
                    this.X + this.w, this.Y + this.t, this.Z + this.w, // 3.
                    // right-top-front
                    // BOTTOM
                    this.X - this.w, this.Y, this.Z - this.w, // 4.
                    // left-bottom-back
                    this.X - this.w, this.Y, this.Z + this.w, // 0.
                    // left-bottom-front
                    this.X + this.w, this.Y, this.Z - this.w, // 6.
                    // right-bottom-back
                    this.X + this.w, this.Y, this.Z + this.w, // 1.
            // right-bottom-front
            };
            vertices = _temp;

            return;// Break out of function, so not to check other orientations
        }
        // Left or right paddles
        if (this.or == 1)
        {
            float[] _temp =
            {
                    // Vertices of the 6 faces
                    // FRONT
                    this.X, this.Y - this.w, this.Z + this.w, // 0.
                    // left-bottom-front
                    this.X + this.t, this.Y - this.w, this.Z + this.w, // 1.
                    // right-bottom-front
                    this.X + this.t, this.Y + this.w, this.Z + this.w, // 3.
                    // right-top-front
                    this.X, this.Y + this.w, this.Z + this.w, // 2.
                    // left-top-front
                    // BACK
                    this.X + this.t, this.Y - this.w, this.Z - this.w, // 6.
                    // right-bottom-back
                    this.X, this.Y - this.w, this.Z - this.w, // 4.
                    // left-bottom-back
                    this.X + this.t, this.Y + this.w, this.Z - this.w, // 7.
                    // right-top-back
                    this.X, this.Y + this.w, this.Z - this.w, // 5.
                    // left-top-back
                    // LEFT
                    this.X, this.Y - this.w, this.Z - this.w, // 4.
                    // left-bottom-back
                    this.X, this.Y - this.w, this.Z + this.w, // 0.
                    // left-bottom-front
                    this.X, this.Y + this.w, this.Z - this.w, // 5.
                    // left-top-back
                    this.X, this.Y + this.w, this.Z + this.w, // 2.
                    // left-top-front
                    // RIGHT
                    this.X + this.t, this.Y - this.w, this.Z + this.w, // 1.
                    // right-bottom-front
                    this.X + this.t, this.Y - this.w, this.Z - this.w, // 6.
                    // right-bottom-back
                    this.X + this.t, this.Y + this.w, this.Z + this.w, // 3.
                    // right-top-front
                    this.X + this.t, this.Y + this.w, this.Z - this.w, // 7.
                    // right-top-back
                    // TOP
                    this.X, this.Y + this.w, this.Z - this.w, // 5.
                    // left-top-back
                    this.X, this.Y + this.w, this.Z + this.w, // 2.
                    // left-top-front
                    this.X + this.t, this.Y + this.w, this.Z - this.w, // 7.
                    // right-top-back
                    this.X + this.t, this.Y + this.w, this.Z + this.w, // 3.
                    // right-top-front
                    // BOTTOM
                    this.X, this.Y - this.w, this.Z - this.w, // 4.
                    // left-bottom-back
                    this.X, this.Y - this.w, this.Z + this.w, // 0.
                    // left-bottom-front
                    this.X + this.t, this.Y - this.w, this.Z - this.w, // 6.
                    // right-bottom-back
                    this.X + this.t, this.Y - this.w, this.Z + this.w, // 1.
            // right-bottom-front
            };
            vertices = _temp;
            return;// Break out of function, so not to check other orientations
        }
        // Front or back paddles
        if (this.or == 2)
        {
            float[] _temp =
            {
                    // Vertices of the 6 faces
                    // FRONT
                    this.X - this.w, this.Y - this.w, this.Z - this.t, // 0.
                    // left-bottom-front
                    this.X + this.w, this.Y - this.w, this.Z - this.t, // 1.
                    // right-bottom-front
                    this.X + this.w, this.Y + this.w, this.Z - this.t, // 3.
                    // right-top-front
                    this.X - this.w, this.Y + this.w, this.Z - this.t, // 2.
                    // left-top-front
                    // BACK
                    this.X + this.w, this.Y - this.w, this.Z, // 6.
                    // right-bottom-back
                    this.X - this.w, this.Y - this.w, this.Z, // 4.
                    // left-bottom-back
                    this.X + this.w, this.Y + this.w, this.Z, // 7.
                    // right-top-back
                    this.X - this.w, this.Y + this.w, this.Z, // 5.
                    // left-top-back
                    // LEFT
                    this.X - this.w, this.Y - this.w, this.Z, // 4.
                    // left-bottom-back
                    this.X - this.w, this.Y - this.w, this.Z - this.t, // 0.
                    // left-bottom-front
                    this.X - this.w, this.Y + this.w, this.Z, // 5.
                    // left-top-back
                    this.X - this.w, this.Y + this.w, this.Z - this.t, // 2.
                    // left-top-front
                    // RIGHT
                    this.X + this.w, this.Y - this.w, this.Z - this.t, // 1.
                    // right-bottom-front
                    this.X + this.w, this.Y - this.w, this.Z, // 6.
                    // right-bottom-back
                    this.X + this.w, this.Y + this.w, this.Z - this.t, // 3.
                    // right-top-front
                    this.X + this.w, this.Y + this.w, this.Z, // 7.
                    // right-top-back
                    // TOP
                    this.X - this.w, this.Y + this.w, this.Z, // 5.
                    // left-top-back
                    this.X - this.w, this.Y + this.w, this.Z - this.t, // 2.
                    // left-top-front
                    this.X + this.w, this.Y + this.w, this.Z, // 7.
                    // right-top-back
                    this.X + this.w, this.Y + this.w, this.Z - this.t, // 3.
                    // right-top-front
                    // BOTTOM
                    this.X - this.w, this.Y - this.w, this.Z, // 4.
                    // left-bottom-back
                    this.X - this.w, this.Y - this.w, this.Z - this.t, // 0.
                    // left-bottom-front
                    this.X + this.w, this.Y - this.w, this.Z, // 6.
                    // right-bottom-back
                    this.X + this.w, this.Y - this.w, this.Z - this.t, // 1.
            // right-bottom-front
            };
            vertices = _temp;
            return;// Break out of function, so not to check other orientations
        }
        return;
    }

    /*
     * Returns true if ball hits paddle
     */
    public boolean checkxyz(float _x, float _y, float _z, float _r)
    {
        // Assume ball misses paddle
        boolean flag = false;

        // Check orientation of the paddle
        switch (this.or)
        {
            case 0:
                // Top/Bottom
                // Moves in XZ, so check X and Z
                if (Math.abs(this.Y - _y) < 0.1)
                {
                    if ((_x <= this.X + this.w) && (_x >= this.X - this.w) && (_z <= this.Z + this.w) && (_z >= this.Z - this.w))
                    {
                        // Now, Check Y
                        /*
                         * Seeing as this is only called on a bounce, can safely compare the Y coordinate of the Paddle and the ball, and
                         * give
                         * 10% leeway(means that we detect the right ball)
                         */
                        // hit
                        flag = true;
                    }
                    else
                    {

                        // Miss
                        this.pPlayer.loseLife();
                    }
                }
                break;

            case 1:
                // Front/Back
                // Check YZ
                if (Math.abs(this.X - _x) < 0.1)
                {
                    if ((_y <= this.Y + this.w) && (_y >= this.Y - this.w) && (_z <= this.Z + this.w) && (_z >= this.Z - this.w))
                    {
                        // Now, check X
                        // hit
                        flag = true;
                    }
                    else
                    {
                        // Miss
                        this.pPlayer.loseLife();
                    }
                }
                break;

            case 2:
                // Left/Right
                // Check XY plane
                if (Math.abs(this.Z - _z) < 0.1)
                {
                    if ((_y <= this.Y + this.w) && (_y >= this.Y - this.w) && (_x <= this.X + this.w) && (_x >= this.X - this.w))
                    {
                        // hit
                        flag = true;
                    }
                    else
                    {
                        this.pPlayer.loseLife();
                    }
                }
                break;
        }
        return flag;
    }

    public int getPaddle_state()
    {
        return paddle_state;
    }

    public void setPaddle_state(int paddle_state)
    {
        this.paddle_state = paddle_state;
    }

    public Player getPlayer()
    {
        return pPlayer;
    }

    public void setPlayer(Player pPlayer)
    {
        this.pPlayer = pPlayer;
        if (pPlayer.getPlayerID() == 1)
        {
            // Player is human
            float[][] nColor =
            {
            { 0.0f, 0.0f, 1.0f, 1.0f },
            { 0.0f, 0.0f, 1.0f, 1.0f },
            { 0.0f, 0.0f, 1.0f, 1.0f },
            { 0.0f, 0.0f, 1.0f, 1.0f },
            { 0.0f, 0.0f, 1.0f, 1.0f },
            { 0.0f, 0.0f, 1.0f, 1.0f } };
            colors = nColor;
        }
        else
        {
            // Player is Computer Controlled, so red
            float[][] nColor =
            {
            { 1.0f, 0.0f, 0.0f, 1.0f },
            { 1.0f, 0.0f, 0.0f, 1.0f },
            { 1.0f, 0.0f, 0.0f, 1.0f },
            { 1.0f, 0.0f, 0.0f, 1.0f },
            { 1.0f, 0.0f, 0.0f, 1.0f },
            { 1.0f, 0.0f, 0.0f, 1.0f } };
            colors = nColor;
        }
    }

    // Resets the paddle back to default state
    public void reset(float x, float y, float z)
    {
        switch (or)
        {
            case 0:
                this.X = x;
                this.Z = z;
                break;
            case 1:
                this.Y = y;
                this.Z = z;
                break;
            case 2:
                this.X = x;
                this.Y = y;
                break;

        }
    }
}
