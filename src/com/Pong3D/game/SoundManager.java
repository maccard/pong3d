package com.Pong3D.game;

import java.util.HashMap;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;

/*
 * Singleton design pattern. only allows one static instance of the class to exist.
 * Returns a reference to the class, if one already exists
 */
public class SoundManager
{

    static private SoundManager _instance;
    private static SoundPool mSoundPool;
    private static HashMap<Integer, Integer> mSoundPoolMap;
    private static AudioManager mAudioManager;
    private static Context mContext;

    private SoundManager()
    {}

    /*
     * @singleton
     * Requests an instance of the class, and creates it if it doesn't exist
     */
    static synchronized public SoundManager getInstance()
    {
        if (_instance == null)
            _instance = new SoundManager();
        return _instance;
    }

    /*
     * Initialises the storage for the sounds
     */
    public static void initSounds(Context theContext)
    {
        mContext = theContext;
        // Soundpool is an android object.
        mSoundPool = new SoundPool(4, AudioManager.STREAM_MUSIC, 0);
        // Store in a hashamp for easy references
        mSoundPoolMap = new HashMap<Integer, Integer>();
        // Lets us choose system volume
        mAudioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
    }

    /*
     * Add a new Sound to the SoundPool
     */
    public static void addSound(int Index, int SoundID)
    {
        mSoundPoolMap.put(Index, mSoundPool.load(mContext, SoundID, 1));
    }

    /*
     * Loads sound files, and puts them into the Hashmap
     */
    public static void loadSounds()
    {
        mSoundPoolMap.put(1, mSoundPool.load(mContext, R.raw.blip, 1));
        mSoundPoolMap.put(2, mSoundPool.load(mContext, R.raw.loselife, 1));
    }

    /**
     * Plays a Sound
     */
    public static void playSound(int index)
    {
        float streamVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        streamVolume = streamVolume / mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        mSoundPool.play((Integer) mSoundPoolMap.get(index), streamVolume, streamVolume, 1, 0, 1.0f);
    }

    /*
     * Stop a Sound
     */
    public static void stopSound(int index)
    {
        mSoundPool.stop((Integer) mSoundPoolMap.get(index));
    }

    /*
     * Deallocates the resources and Instance of SoundManager
     * Used as this is Singleton
     */
    public static void cleanup()
    {
        mSoundPool.release();
        mSoundPool = null;
        mSoundPoolMap.clear();
        mAudioManager.unloadSoundEffects();
        _instance = null;

    }

}