package com.Pong3D.game;

import android.app.Activity;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;

/*
 * Filters out the gestures that we don't need!
 */
public class SimpleGestureFilter extends SimpleOnGestureListener
{

    // Variables to do with direction of gesture
    public final static int SWIPE_UP = 1;
    public final static int SWIPE_DOWN = 2;
    public final static int SWIPE_LEFT = 3;
    public final static int SWIPE_RIGHT = 4;

    public final static int MODE_TRANSPARENT = 0;
    public final static int MODE_SOLID = 1;
    public final static int MODE_DYNAMIC = 2;

    private final static int ACTION_FAKE = -1337; // It's an escape clause

    // Couple of safety checks here, to make sure it's a gesture
    private int swipe_Min_Distance = 100;
    private int swipe_Max_Distance = 350;
    private int swipe_Min_Velocity = 100;

    // initialise checks
    private int mode = MODE_DYNAMIC;
    private boolean running = true;
    private boolean tapIndicator = false;

    // Stores the activity, a gesturedetector and the listener
    private Activity context;
    private GestureDetector detector;
    private SimpleGestureListener listener;

    /*
     * DEfault constructor, stores the activity and creates new listener and detector
     */
    public SimpleGestureFilter(Activity context, SimpleGestureListener sgl)
    {

        this.context = context;
        this.detector = new GestureDetector(context, this);
        this.listener = sgl;
    }

    // THis overrides the OnTouch Event. This is received instead of in the MyGLSurfaceView
    public void onTouchEvent(MotionEvent event)
    {

        // If program isn't running, then exit.
        if (!this.running)
            return;

        // Check if event has happened
        boolean result = this.detector.onTouchEvent(event);

        // Detect the event, and find the right direction
        if (event.getAction() == ACTION_FAKE)
            event.setAction(MotionEvent.ACTION_UP);
        else
            if (result)
                event.setAction(MotionEvent.ACTION_CANCEL);
            else
                if (this.tapIndicator)
                {
                    event.setAction(MotionEvent.ACTION_DOWN);
                    this.tapIndicator = false;
                }

        // else just do nothing, it's Transparent
    }

    /*
     * Eclipse-Generated methods
     */
    public void setMode(int m)
    {
        this.mode = m;
    }

    public int getMode()
    {
        return this.mode;
    }

    public void setEnabled(boolean status)
    {
        this.running = status;
    }

    public void setSwipeMaxDistance(int distance)
    {
        this.swipe_Max_Distance = distance;
    }

    public void setSwipeMinDistance(int distance)
    {
        this.swipe_Min_Distance = distance;
    }

    public void setSwipeMinVelocity(int distance)
    {
        this.swipe_Min_Velocity = distance;
    }

    public int getSwipeMaxDistance()
    {
        return this.swipe_Max_Distance;
    }

    public int getSwipeMinDistance()
    {
        return this.swipe_Min_Distance;
    }

    public int getSwipeMinVelocity()
    {
        return this.swipe_Min_Velocity;
    }

    // END GETTERS & SETTERS

    /*
     * This is an override of the onfling event, adapted to work
     * @see android.view.GestureDetector.SimpleOnGestureListener#onFling(android.view.MotionEvent, android.view.MotionEvent, float, float)
     */
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
    {

        // Get the distance travelled in X and Y direction
        final float xDistance = Math.abs(e1.getX() - e2.getX());
        final float yDistance = Math.abs(e1.getY() - e2.getY());

        // If either distance is larger than the max distance, then don't trigger gesture
        if (xDistance > this.swipe_Max_Distance || yDistance > this.swipe_Max_Distance)
            return false;

        // Absolute velocities of the flick
        velocityX = Math.abs(velocityX);
        velocityY = Math.abs(velocityY);

        // Toggle Flag
        boolean result = false;

        /*
         * THis is only entered if the X velocity is high enough, and X distance is far enough
         */
        if (velocityX > this.swipe_Min_Velocity && xDistance > this.swipe_Min_Distance)
        {
            // Check direction
            if (e1.getX() > e2.getX()) // right to left
                this.listener.onSwipe(SWIPE_LEFT);
            else
                this.listener.onSwipe(SWIPE_RIGHT);

            result = true;
        }
        else
            /*
             * Same as above except in Y direction
             */
            if (velocityY > this.swipe_Min_Velocity && yDistance > this.swipe_Min_Distance)
            {
                if (e1.getY() > e2.getY()) // bottom to up
                    this.listener.onSwipe(SWIPE_UP);
                else
                    this.listener.onSwipe(SWIPE_DOWN);

                result = true;
            }

        return result;
    }

    /*
     * The following code was compiled by myself t0mless, and DrPenguin from the Android Developers IRC channel.
     */

    /*
     * Handles a single tap on the screen, and does nothing to it. Overrides default behavoiur
     * @see android.view.GestureDetector.SimpleOnGestureListener#onSingleTapUp(android.view.MotionEvent)
     */
    @Override
    public boolean onSingleTapUp(MotionEvent e)
    {
        this.tapIndicator = true;
        return false;
    }

    /*
     * Handles a double tap on the screen, and does nothing to it. Overrides default behavoiur
     * @see android.view.GestureDetector.SimpleOnGestureListener#onDoubleTap(android.view.MotionEvent)
     */
    @Override
    public boolean onDoubleTap(MotionEvent arg0)
    {
        this.listener.onDoubleTap();
        return true;
    }

    /*
     * Again, overrides default behaviour incase anything is specified anywhere else
     * @see android.view.GestureDetector.SimpleOnGestureListener#onDoubleTapEvent(android.view.MotionEvent)
     */
    @Override
    public boolean onDoubleTapEvent(MotionEvent arg0)
    {
        return true;
    }

    /*
     * Causes a notification when a single tap happens. this is converted to an up swipe under certain conditions
     * @see android.view.GestureDetector.SimpleOnGestureListener#onSingleTapConfirmed(android.view.MotionEvent)
     */
    @Override
    public boolean onSingleTapConfirmed(MotionEvent arg0)
    {

        if (this.mode == MODE_DYNAMIC)
        { // we owe an ACTION_UP, so we fake an
            arg0.setAction(ACTION_FAKE); // action which will be converted to an ACTION_UP later.
            this.context.dispatchTouchEvent(arg0);
        }

        return false;
    }

    static interface SimpleGestureListener
    {
        void onSwipe(int direction);

        void onDoubleTap();

    }

}
