package com.Pong3D.game;

import android.app.Activity;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.WindowManager;

import com.Pong3D.game.SimpleGestureFilter.SimpleGestureListener;

// Main game activity
public class Pong3D extends Activity implements SimpleGestureListener
{
    private GLSurfaceView glView; // Use GLSurfaceView
    Game _game;
    private SimpleGestureFilter detector;
    private Render renderer;

    // Call back when the activity is started, to initialise the view
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // Sound Manager
        // Singleton pattern
        SoundManager.getInstance();
        SoundManager.initSounds(this);
        SoundManager.loadSounds();
        // GESTURE FILTER
        detector = new SimpleGestureFilter(this, this);
        // END GESTURES

        // Creates a new instance of the game. Done so that we can have the menu ingame
        _game = new Game(this);
        // Create a new renderer with this games type

        renderer = new Render(_game);
        /*
         * Instead of default GLSurfaceView, use a custom one. Used to override
         * the onkey and ontouch events
         * Also, pass the game to this, so it can be created in the custom renderer
         */
        glView = new MyGLSurfaceView(this, _game, renderer); // Allocate a GLSurfaceView
        this.setContentView(glView); // This activity sets to MyGLSurfaceView

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    // Call back when the activity is going into the background
    @Override
    protected void onPause()
    {
        // So that the main game view closes when the app closes, and when the game ends
        finish();
        super.onPause();
        glView.onPause();
    }

    // Call back after onPause()
    @Override
    protected void onResume()
    {
        super.onResume();
        glView.onResume();
    }

    // Menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Draw layout form XML
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.layout.gamemenu, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Execute menu selected items
        switch (item.getItemId())
        {
            case R.id.restart:
                // Restart game
                _game.newGame();
                break;
            case R.id.exit:
                SoundManager.cleanup();
                this.finish();
                break;
            case R.id.none:
                renderer.getGame().paddle_1.setPaddle_state(1);
                renderer.getGame().paddle_6.setPaddle_state(1);
                break;
            case R.id.one:
                renderer.getGame().paddle_1.setPaddle_state(0);
                renderer.getGame().paddle_6.setPaddle_state(1);
                break;
            case R.id.two:
                renderer.getGame().paddle_1.setPaddle_state(0);
                renderer.getGame().paddle_6.setPaddle_state(0);
                break;
        }
        return true;
    }

    // FROM HERE ON IN, IS EXPERIMENTAL GESTURES

    // This allows the activity to handle the ontouch event rather than the view(which doesn't exist)
    @Override
    public boolean dispatchTouchEvent(MotionEvent me)
    {
        this.detector.onTouchEvent(me);
        return super.dispatchTouchEvent(me);
    }

    @Override
    public void onSwipe(int direction)
    {
        switch (direction)
        {

            case SimpleGestureFilter.SWIPE_RIGHT:
                this.renderer.newy += 90;
                break;
            case SimpleGestureFilter.SWIPE_LEFT:
                this.renderer.newy -= 90;
                break;
            case SimpleGestureFilter.SWIPE_DOWN:
                this.renderer.newx += 90;
                break;
            case SimpleGestureFilter.SWIPE_UP:
                this.renderer.newx -= 90;
                break;

        }
    }

    @Override
    public void onDoubleTap()
    {
        // Log.v("Tapped", "Double");
    }

}