package com.Pong3D.game;

import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

/*
 * Holds all the game logic, and is called from the renderer.
 */
public class Game
{
    // The Pong3D activity
    private Context cxt;

    // Main game cube
    private Cube cube;

    // Players
    public Player player1;
    public Player player2;

    // Paddles. Numbers are associated with the sides of the cube they are on
    public Paddle paddle_1; // Player 1
    public Paddle paddle_2; // Player 2
    public Paddle paddle_3; // Player 1
    public Paddle paddle_4; // Player 2
    public Paddle paddle_5; // Player 1
    public Paddle paddle_6; // Player 2

    // Ball
    private Ball ball;

    float bx, by, bz;

    public Game(Context _cxt)
    {
        cxt = _cxt;
        // Set up the buffers for these shapes
        cube = new Cube(); // (NEW)
        // Buffering Paddles
        paddle_1 = new Paddle(0.0f, -1.0f, 0.0f, 0, 0);// Bottom
        paddle_2 = new Paddle(0.0f, 0.0f, 1.0f, 2, 1);// Front
        paddle_3 = new Paddle(1.0f, 0.0f, 0.0f, 1, 1);// Right
        paddle_4 = new Paddle(-1.0f, 0.0f, 0.0f, 1, 1);// Left
        paddle_5 = new Paddle(0.0f, 0.0f, -1.0f, 2, 1);// Back
        paddle_6 = new Paddle(0.0f, 1.0f, 0.0f, 0, 1);// Top

        // Assign PlayerID's
        player1 = new Player(1);
        player2 = new Player(2);

        // Assign paddles to players
        paddle_1.setPlayer(player1);
        paddle_3.setPlayer(player1);
        paddle_5.setPlayer(player1);
        paddle_2.setPlayer(player2);
        paddle_4.setPlayer(player2);
        paddle_6.setPlayer(player2);

        // Buffer Ball
        // Pass it X Y Z coordinates
        ball = new Ball(0.0f, 0.0f, 0.0f);

    }

    // This function contains the game logic
    void updateGame()
    {
        // Get ball position
        float bx = ball.getX(), by = ball.getY(), bz = ball.getZ();

        /*
         * Automatic moving of the paddles
         * If paddle state is 1, paddle is player controlled
         * Otherwise, paddle is user controlled
         */

        if (paddle_1.getPaddle_state() == 1)
            paddle_1.updatePaddle(bx, by, bz);
        if (paddle_2.getPaddle_state() == 1)
            paddle_2.updatePaddle(bx, by, bz);
        if (paddle_3.getPaddle_state() == 1)
            paddle_3.updatePaddle(bx, by, bz);
        if (paddle_4.getPaddle_state() == 1)
            paddle_4.updatePaddle(bx, by, bz);
        if (paddle_5.getPaddle_state() == 1)
            paddle_5.updatePaddle(bx, by, bz);
        if (paddle_6.getPaddle_state() == 1)
            paddle_6.updatePaddle(bx, by, bz);
        // Get the orientation of the bounce of the ball(if at all)
        int bounce = ball.updateBall();

        // Check if ball bounces, and whether it hits a paddle
        if (bounce != -1 && !game_over(ball.getX(), ball.getY(), ball.getZ(), bounce))
        {

            // Log.v("P1", "" + player1.getLives());
            // Log.v("P2", "" + player2.getLives());
            // If both players are still alive
            if (player1.isAlive() && player2.isAlive())
            {
                restart();
            }
            else
            {
                // One or both are dead
                if (player1.isAlive())
                {
                    // You have won
                    endgame(player1);

                }
                if (player2.isAlive())
                {
                    // You have lost
                    endgame(player2);

                }
                // If you get to here, somethings wrong...
            }
            // Stop the activity
            // finish();
        }
    }

    // This draws after all calculations have been done
    void drawGame(GL10 gl)
    {
        // Draw the cube
        cube.draw(gl);

        // Draw paddles after cube, so that they will superimpose on the cube
        paddle_1.draw(gl);
        paddle_2.draw(gl);
        paddle_3.draw(gl);
        paddle_4.draw(gl);
        paddle_5.draw(gl);
        paddle_6.draw(gl);

        // Draw the ball
        ball.draw(gl);

    }

    /*
     * This function takes in an X Y Z Coordinate of a ball returns true if ball
     * has hit a paddle, false if it misses a paddle
     * If there's a miss, it removes a life from the respective player
     * or is the orientation of the bounce
     */

    private boolean game_over(float _x, float _y, float _z, int or)
    {
        float _r = ball.getRadius();
        // Assume ball hits paddle
        boolean flag = false;

        // Same thing for each orientation
        /*
         * note that each orientation only checks two paddles
         */
        switch (or)
        {
        // Means ball didnt bounce
            case -1:
                flag = true;
                break;

            case 0:
                // Top or bottom
                if (paddle_1.checkxyz(_x, _y, _z, _r))
                {
                    // Player 1
                    flag = true;
                    break;
                }
                if (paddle_6.checkxyz(_x, _y, _z, _r))
                {
                    // Player 2
                    flag = true;
                    break;
                }
                break;

            case 1:
                // Left or right
                if (paddle_3.checkxyz(_x, _y, _z, _r))
                {
                    // Player 1
                    flag = true;
                    break;
                }
                if (paddle_4.checkxyz(_x, _y, _z, _r))
                {
                    // Player 2
                    flag = true;
                    break;
                }
                break;

            case 2:
                // Front or back
                if (paddle_2.checkxyz(_x, _y, _z, _r))
                {
                    // Player 2
                    flag = true;
                    break;
                }
                if (paddle_5.checkxyz(_x, _y, _z, _r))
                {
                    // Player 1
                    flag = true;
                    break;
                }
                break;
        }
        // Play respective sound
        if (flag)
            SoundManager.playSound(1);
        else
            SoundManager.playSound(2);

        return flag;

    }

    // Resets paddles, ball, and serves ball
    public void restart()
    {
        // Reset ball
        ball.reset();

        // Paddles do not need to be reset as they follow the ball.

        // Serve ball
        ball.serveBall();

    }

    /*
     * Launches new splash screen for winner
     */
    public void endgame(Player winner)
    {
        Intent i;
        if (winner == player1)
        {
            Log.v("Game Over", "Win");
            i = new Intent(cxt, Winner.class);
        }
        else
        {
            Log.v("Game Over", "Lose");
            i = new Intent(cxt, Loser.class);
        }
        cxt.startActivity(i);

    }

    public void newGame()
    {
        // reposition everything
        restart();
        // Reset players lives
        player1.setLives(3);
        player2.setLives(3);
    }

}
