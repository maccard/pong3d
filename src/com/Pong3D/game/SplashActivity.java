package com.Pong3D.game;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

// Renders the splash screen with logo, my student number and also my name
public class SplashActivity extends Activity
{
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        // Use XML Layout
        setContentView(R.layout.splash);
        // New Thread
        Thread splashThread = new Thread()
        {
            @Override
            public void run()
            {
                try
                {
                    // Foobar
                }
                finally
                {
                    // Close splash screen activity
                    finish();
                    // Launch new activity
                    Intent i = new Intent(SplashActivity.this, MenuActivity.class);
                    startActivity(i);
                }
            }
        };
        splashThread.start();
    }
}