package com.Pong3D.game;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;

/*
 * This is a touch to continue activity, so implements onTouch, and if it is touched, it moves to the next activity
 */
public class Loser extends Activity
{
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        // Loads view from menu.xml
        setContentView(R.layout.lose);

        // Creates a new thread, and runs it forever.
        Thread splashThread = new Thread()
        {
            @Override
            public void run()
            {
                int waited = 0;
                while (waited < 1)
                {
                    // Infinite Loop
                }
            }

        };
        splashThread.start();
    }

    public boolean onTouchEvent(MotionEvent event)
    {
        finish();
        // Launch new activity
        Intent i = new Intent(Loser.this, MenuActivity.class);
        startActivity(i);
        return true;
    }
}
