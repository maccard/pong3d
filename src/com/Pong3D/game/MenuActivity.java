package com.Pong3D.game;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Display;
import android.view.MotionEvent;

/*
 * Renders Main menu
 * Draws image from XML layout in infinite loop until a button is touched
 */
public class MenuActivity extends Activity
{
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        // Loads view from menu.xml
        setContentView(R.layout.menu);

        // Creates a new thread, and runs it forever.
        Thread splashThread = new Thread()
        {
            @Override
            public void run()
            {
                int waited = 0;
                while (waited < 1)
                {
                    // Infinite Loop
                }
            }
        };
        splashThread.start();
    }

    // If the user touches the screen, this will be called
    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        Display display = getWindowManager().getDefaultDisplay();
        int sw = display.getWidth(); // deprecated
        int sh = display.getHeight(); // deprecated

        // Get XY coordinates of Touch
        int x = (int) event.getX();
        int y = (int) event.getY();

        int action = event.getAction();
        switch (action)
        {
            case (MotionEvent.ACTION_DOWN): // Touch screen pressed
                if ((x > sw * .4) && (x < sw * .8) && (y > sh * .52) && (y < sh * .61))
                    // User Touched Start game
                    this.startgame();
                if ((x > sw * .4) && (x < sw * .8) && (y > sh * .68) && (y < sh * .77))
                    // User Touched Exit
                    this.quitgame();
                break;
            case (MotionEvent.ACTION_UP): // Touch screen touch ended
                break;
            case (MotionEvent.ACTION_MOVE): // Contact has moved across screen
                break;
            case (MotionEvent.ACTION_CANCEL): // Touch event cancelled
                break;
        }
        return super.onTouchEvent(event);
    }

    private void startgame()
    {
        // If this line is uncommented, when back is pressed, the user will exit
        // the app.Right now, it brings you back to the main menu :)

        // finish();

        // Launch Game Activity
        Intent i = new Intent(MenuActivity.this, Pong3D.class);
        startActivity(i);
    }

    private void quitgame()
    {
        // Clean up sounds
        SoundManager.cleanup();
        // Close Menu
        finish();
    }
}
