package com.Pong3D.game;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

public class Ball
{

    static private FloatBuffer sphereVertex;// Vertex Buffer

    static float sphere_parms[] = new float[3];
    public float mRadius = .06f;// Radius of sphere

    // Defines size of each face. Higher number yields better performance, but
    // renders shape less spherical. 25 seems fine. 30 looks shoddy, 20 gets
    // slow
    double mStep = 25;
    float mVertices[];// Vertex array to buffer
    private static double DEG = Math.PI / 180;// Math constant for converting
    // degrees to radians
    private float colors[] =
    { 1.0f, 1.0f, 1.0f, 1.0f };// Colour of ball

    // Number of points drawn.
    int mPoints;

    // This is the centre point of the ball
    private float X, Y, Z;

    // Velocity of the ball in 3 dimensions
    private float vx = 0, vy = 0, vz = 0;

    public Ball(float _x, float _y, float _z)
    {
        // Set initial XYZ position(usually 0 0 0)
        this.X = _x;
        this.Y = _y;
        this.Z = _z;

        // Serve the ball
        this.serveBall();

        // Setup vertex array buffer.
        ByteBuffer vbb = ByteBuffer.allocateDirect(40000);
        vbb.order(ByteOrder.nativeOrder()); // Use native byte order
        sphereVertex = vbb.asFloatBuffer(); // Convert from byte to float
        // generate sphere and store number of points(note this is proportional
        // to mStep);
        mPoints = build();
    }

    /*
     * returns radius - Eclipse Generated
     */
    public float getRadius()
    {
        return this.mRadius;
    }

    // Updates the velocity of the ball.
    public void velocity(float _vx, float _vy, float _vz)
    {
        this.vx = _vx;
        this.vy = _vy;
        this.vz = _vz;
    }

    /*
     * Serve the ball
     */
    public void serveBall()
    {
        // Set ball position back to centre of the cube
        this.X = 0;
        this.Y = 0;
        this.Z = 0;

        // Pause for one second
        try
        {
            Thread.sleep(1000);
        }
        catch (InterruptedException e)
        {
            // no catch
        }

        // Set the velocity to a random variable between 0 and .08f, as any higher is too fast
        this.vx = (float) (Math.random() / 12.5);
        this.vy = (float) (Math.random() / 12.5);
        this.vz = (float) (Math.random() / 12.5);
    }

    /*
     * This function updates the ball position. Sets the new position, and
     * checks if the new position lies within the cube. if it does, happy days,
     * otherwise ball must bounce(flip velocity in the offending direction by
     * 180 degrees)
     * returns the orientation of the bounce or -1 if it didn't bounce
     */
    public int updateBall()
    {
        // Assume it misses
        int flag = -1;

        // Move

        this.X += this.vx;
        this.Y += this.vy;
        this.Z += this.vz;
        // Check if ball is outside boundaries, if it is, flip the velocity
        // direction
        if (this.X + this.mRadius >= 1.0f || this.X - this.mRadius <= -1.0f)
        {
            this.vx *= -1.;
            flag = 1;

        }

        if (this.Y + this.mRadius >= 1.0f || this.Y - this.mRadius <= -1.0f)
        {
            this.vy *= -1.;
            flag = 0;
        }

        if (this.Z + this.mRadius >= 1.0f || this.Z - this.mRadius <= -1.0f)
        {
            this.vz *= -1.;
            flag = 2;
        }

        return flag;
    }

    public void draw(GL10 gl)
    {
        // Rebuild the sphere at the new point.
        mPoints = build();
        gl.glFrontFace(GL10.GL_CW);
        gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
        gl.glVertexPointer(3, GL10.GL_FLOAT, 0, sphereVertex);

        gl.glColor4f(colors[0], colors[1], colors[2], colors[3]);

        gl.glDrawArrays(GL10.GL_TRIANGLE_FAN, 0, mPoints);
        gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
    }

    /*
     * This function generates the sphere. Does all the math etc needed, but is
     * very inefficient. Returns the number of points calculated on the
     * sphere(bearing in mind that each point is a combination of three
     * coordinates)
     */
    private int build()
    {

        double dTheta = mStep * DEG;
        double dPhi = dTheta;
        int points = 0;

        /*
         * Calulate the sphere points. Note that the density of the points is
         * larger at certain points of the sphere, so this is very inefficient
         */
        for (double phi = -(Math.PI); phi <= Math.PI; phi += dPhi)
        {
            // Note this is only to PI, not the whole sphere. Difference visually isn't much, but performance benefits hugely from it
            for (double theta = 0.0; theta <= (Math.PI); theta += dTheta)
            { // equally sample the sphere. Not nice but works for now.
              // X coordinate
                sphereVertex.put((float) (mRadius * Math.sin(phi) * Math.cos(theta)) + X);
                // Y Coordinate
                sphereVertex.put((float) (mRadius * Math.sin(phi) * Math.sin(theta)) + Y);
                // Z Coordinate
                sphereVertex.put((float) (mRadius * Math.cos(phi)) + Z);
                points++;
            }
        }
        // Reset the vertex buffer back to 0;
        sphereVertex.position(0);
        // Return number of points generated
        return points;
    }

    /*
     * This function stops the ball.
     */
    public void reset()
    {
        this.vx = this.vy = this.vz = 0;
        this.X = this.Y = this.Z = 0;
    }

    /*
     * Rest of functions are eclipse generated get and set methods
     */
    public float getvx()
    {
        return vx;
    }

    public float getvy()
    {
        return vy;
    }

    public float getvz()
    {
        return vz;
    }

    public float getX()
    {
        return X;
    }

    public float getY()
    {
        return Y;
    }

    public float getZ()
    {
        return Z;
    }
}
